import numpy as np
import matplotlib.pyplot as plt
import analyse
import os

properties = {'mother': 0,'alive': 1,'energy': 2,'flavour': 3,
              'momentum': 4,'type': 5}
quark_types = ['u','d','s','c','b','NaN']
colormap = {'q':'r','g':'b'}
stylemap = {'q':'-','g':'--'}

def draw_line(typ,x1,y1,x2,y2,ax):
    ax.plot([x1,x2],[y1,y2],color=colormap[typ],linestyle=stylemap[typ])

for f in os.listdir('./large_sts/'):
    if f.find('.dat') != -1:
        name = f.replace('.dat','')
        name = name.replace('records_','')
        qtype = name.split('_')[0]
        E0 = name.split('_')[1]
        #print(qtype,E0)
        if qtype in ['NaN','c','u'] and float(E0) < 5000:
            anal = analyse.analysis(int(float(E0)),folder='./large_sts/',flavour=qtype,SAVE_ANG=False,SAVECONESTUFF = False,read1ev = True)
            list_of_particles = anal.map[1]
            index_to_pos = {}
            index_to_pos[0] = [0.,0.]
            i = 0
            fig = plt.figure(figsize=(30,30))
            ax = fig.add_subplot(111)
            ax.plot([0,0],[0,0],color=colormap['q'],linestyle=stylemap['q'],label='q')
            ax.plot([0,0],[0,0],color=colormap['g'],linestyle=stylemap['g'],label='g')
            draw_line(list_of_particles[0][properties['type']],-1,0,0,0,ax)
            mothers_found = set()
            for part in list_of_particles:
                mother = part[properties['mother']]
                energy = part[properties['energy']]
                momentum = part[properties['momentum']]
                typ = part[properties['type']]
                if mother != -1:
                    mothers_E = list_of_particles[mother][properties['energy']]
                    mothers_pos = index_to_pos[mother]
                    mothers_mom = list_of_particles[mother][properties['momentum']]
                    delta_p = list(np.array(momentum) - np.array(mothers_mom))
                    absDp = np.sqrt(delta_p[0]**2+delta_p[1]**2+delta_p[2]**2)
                    absDe = np.abs(mothers_E - energy)
                    #new_pos = [mothers_pos[0]+absDe,mothers_pos[1]+absDp]
                    delta = 1./(i)
                    if mother in mothers_found:
                        new_pos = [mothers_pos[0]+1.,mothers_pos[1]-delta]
                    else:
                        mothers_found.add(mother)
                        new_pos = [mothers_pos[0]+1.,mothers_pos[1]+delta]
                    index_to_pos[i] = new_pos
                    draw_line(typ,mothers_pos[0],mothers_pos[1],new_pos[0],new_pos[1],ax)
                i+=1
            ax.legend(prop={'size': 24})
            if qtype == 'NaN': qtype = 'g'
            ax.set_title('E0 = '+E0+' | initial q = '+qtype, fontsize=24)
            fig.savefig('shower_'+qtype+'_'+E0+'.png')