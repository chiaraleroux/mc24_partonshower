import numpy as np
import matplotlib.pyplot as plt
import time

properties = {'mother': 0,'alive': 1,'energy': 2,'flavour': 3,
              'momentum': 4,'type': 5}
quark_types = ['u','d','s','c','b','NaN']

def read_part_line(line):
    props_list = line.replace('mother = ','').replace('alive = ','').replace('energy = ','').replace('flavour = ','').replace('momentum = ','').replace('type = ','').split('|')
    for ind in list(properties.values()):
        if ind == 0: props_list[ind] = int(props_list[ind]) #mother is int
        elif ind == 1: #alive is bool
            if props_list[ind].find('True'): props_list[ind] = True
            else: props_list[ind] = False
        elif ind == 2: props_list[ind] = float(props_list[ind]) #energy is float
        elif ind == 3: props_list[ind] = props_list[ind].strip() #flavour is str
        elif ind == 4: #momentum is a list of floats
            momentum = props_list[ind].replace('[','').replace(']','')
            momentum = momentum.replace(',','').strip()
            momentum = momentum.split(' ')
            for i in range(len(momentum)): momentum[i] = float(momentum[i])
            props_list[ind] = momentum
        elif ind == 5: props_list[ind] = props_list[ind].strip('\n').strip() #type is str
    #print(props_list)
    return props_list #[mother,alive,energy,flavour,momentum,type]

class analysis:
    def __init__(self,Energy = 100,flavour = '',folder = './',SAVE_E = False,SAVE_ANG = False,SAVE_MOM = False,SAVEALL = False,SAVECONESTUFF = False,read1ev = False):
        ### save the data on a map that gives the "list of particles" for a give event, where a "particle" is a lista with its properties
        if flavour != '': filename = (folder+'records_'+flavour+'_'+str(Energy)+'.0.dat')
        else: filename = (folder+'records_'+str(Energy)+'.0.dat')
        record_file = open(filename,'r')
        map_ev_to_list_part_props = {}
        ev = 0
        #particles_list = []
        print('reading file ',filename)
        start_time = time.perf_counter()
        count = 0
        for line in record_file:
            if read1ev and ev > 1: break
            count += 1
            if count%1000000 == 0: print('reading line ',count)
            if line.find('mother = -1') != -1: ## entering new event line block
                same_Event = False # update the control for in or out of event block
            else:
                same_Event = True
            if not same_Event:
                ev += 1 # update the event number
                if ev > 1:
                    map_ev_to_list_part_props[ev - 1] = particles_list
                particles_list = []
                particles_list.append(read_part_line(line)) # add mother
            elif line.find('END') != -1:
                map_ev_to_list_part_props[ev] = particles_list
            else:
                particles_list.append(read_part_line(line))
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        print('done reading file (',elapsed_time,')')
        self.map = map_ev_to_list_part_props
        self.num_evs = len(self.map)
        self.Energy = Energy
        self.SAVE_E = SAVE_E
        self.SAVE_ANG = SAVE_ANG
        self.SAVE_MOM = SAVE_MOM
        self.SAVEALL = SAVEALL
        self.SAVECONESTUFF = SAVECONESTUFF
        self.initial_flavour = flavour

    def get_num_avg_splits(self):
        num_of_events = len(self.map)
        avg_splits = 0
        for ev in self.map:
            num_splits = (len(self.map[ev]) - 1)/2
            avg_splits += num_splits
        avg_splits /= num_of_events
        return avg_splits

    def get_numbers_of_splits(self,Event):
        part_ind = 1
        num_qqg = 0
        num_ggg = 0
        num_gqq = 0
        while part_ind < len(self.map[Event]):
            #print(part_ind,self.map[Event][part_ind])
            daughterB = self.map[Event][part_ind]
            daughterC = self.map[Event][part_ind + 1]
            motherB = daughterB[0]
            motherC = daughterC[0]
            if motherB == motherC:
                mother_type = self.map[Event][motherB][-1].strip()
                dBtype = daughterB[-1].strip()
                dCtype = daughterC[-1].strip()
                splt_type = mother_type + dBtype + dCtype
            else:
                print('something wrong is not right')
                splt_type = 'NaN'
            if splt_type == 'qqg':
                num_qqg += 1
            elif splt_type == 'ggg':
                num_ggg += 1
            elif splt_type == 'gqq':
                num_gqq += 1
            part_ind += 2
        total_num_splits = num_qqg + num_ggg + num_gqq
        #print(total_num_splits,num_qqg,num_ggg,num_gqq)
        return total_num_splits, num_qqg, num_ggg, num_gqq ## don't change order!

    def get_num_avg_splits_of_type(self,tp):
        print('\n\ngetting num avg split of type ',tp)
        type_to_ind = {'tot': 0, 'qqg': 1, 'ggg': 2, 'gqq': 3}
        type_ind = type_to_ind[tp]
        num_events = len(self.map)
        avg_splits = 0
        for event in range(1,num_events+1):
            avg_splits += self.get_numbers_of_splits(event)[type_ind]
        avg_splits /= num_events
        return avg_splits

    def print_splits(self):
        avg_splits = self.get_num_avg_splits()
        print('Einit = ',self.Energy,' => avgsplts = ',avg_splits)
        avg_splits = self.get_num_avg_splits_of_type('tot')
        print('Einit = ',self.Energy,' => avgsplts tot = ',avg_splits)
        avg_splits = self.get_num_avg_splits_of_type('qqg')
        print('Einit = ',self.Energy,' => avgsplts qqg = ',avg_splits)
        avg_splits = self.get_num_avg_splits_of_type('ggg')
        print('Einit = ',self.Energy,' => avgsplts ggg = ',avg_splits)
        avg_splits = self.get_num_avg_splits_of_type('gqq')
        print('Einit = ',self.Energy,' => avgsplts gqq = ',avg_splits)

    ### flavour distribution of final state quarks
    def get_num_flavour_per_ev(self,fl,ev):
        num = 0
        part_list = self.map[ev]
        for part in part_list:
            fl_ind = properties['flavour']
            alive_ind = properties['alive']
            flavour = part[fl_ind]
            alive = part[alive_ind]
            if alive and flavour == fl:
                num += 1
        return num

    def get_avg_num_flavour(self,fl):
        avg_num = 0.
        num_events = len(self.map)
        for ev in range(1,num_events+1):
            avg_num += self.get_num_flavour_per_ev(fl,ev)
        avg_num /= num_events
        return avg_num

    ### get number of final state particles
    def get_num_alive_perEv(self,ev):
        alive_ind = properties['alive']
        num = 0
        part_list = self.map[ev]
        for part in part_list:
            alive = part[alive_ind]
            if alive:
                num += 1
        return num

    def get_avg_FS(self):
        num_evt = len(self.map)
        avg = 0.
        for ev in range(1,num_evt+1):
            avg += self.get_num_alive_perEv(ev)
        avg /= num_evt
        return avg

    def print_fractions(self):
        totalFS = self.get_avg_FS()
        print('average number of FS particles: ',totalFS)
        for flavour in quark_types:
            avg = self.get_avg_num_flavour(flavour)
            print('q type = ',flavour,' -> ',avg,' => ',(avg/totalFS)*100,'%')

    #### check energy distribution
    def get_energies_flav_ev(self,flav,ev):
        fl_ind = properties['flavour']
        alive_ind = properties['alive']
        energy_ind = properties['energy']
        part_list = self.map[ev]
        list_of_energies = []
        for part in part_list:
            alive = part[alive_ind]
            fl = part[fl_ind]
            energy = part[energy_ind]
            if alive and fl == flav:
                list_of_energies.append(energy)
            elif alive and flav == 'any':
                list_of_energies.append(energy)
        return list_of_energies

    def get_energies_dist_flav(self,flav):
        num_evt = len(self.map)
        spec = []
        for ev in range(1,num_evt+1):
            spec += self.get_energies_flav_ev(flav,ev)
        return spec

    def energy_spec(self):
        FIG1 = 'energy_spectrum'
        fig1 = plt.figure(FIG1)
        for fl in quark_types:
            energies = self.get_energies_dist_flav(fl)
            if fl == 'NaN': fl = 'g'
            plt.hist(energies,bins=20,histtype='step',label=fl,density = True)
        plt.title('E0 = '+str(self.Energy))
        plt.legend()
        plt.yscale('log')
        plt.xlabel('energy (GeV)')
        if self.SAVE_E: fig1.savefig(FIG1+'_'+str(self.Energy)+'.png')

    #### let's look at momentum.....? O.O'
    def get_momenta_flav_ev(self,fl,ev):
        fl_ind = properties['flavour']
        alive_ind = properties['alive']
        mom_ind = properties['momentum']
        part_list = self.map[ev]
        list_of_mom = []
        for part in part_list:
            alive = part[alive_ind]
            flav = part[fl_ind]
            mom = part[mom_ind]
            if alive and (flav == fl or fl == 'any'):
                list_of_mom.append(mom)
        return list_of_mom

    def get_mom_dist_flav(self,fl):
        num_evt = len(self.map)
        full_mom_list = []
        for ev in range(1,num_evt+1):
            full_mom_list += self.get_momenta_flav_ev(fl,ev)
        return full_mom_list

    def do_mom_stuff(self):
        FIG2 = 'momentum'
        fig2, ax = plt.subplots(2,2,figsize=(16, 16))
        axes = [ax[0][0],ax[0][1],ax[1][0]]
        for fl in quark_types:
            momenta = self.get_mom_dist_flav(fl)
            list_pabs = []
            for ax_ind in range(3):
                mom_comp_list = []
                for mom in momenta:
                    if ax_ind == 0:
                        list_pabs.append(np.sqrt(mom[ax_ind]**2 + mom[1]**2 + mom[2]**2))
                    mom_comp_list.append(mom[ax_ind])
                if fl == 'NaN': fl = 'g'
                axes[ax_ind].hist(mom_comp_list,bins=30,histtype='step',label=fl+' | p_'+str(ax_ind),density = True)
                axes[ax_ind].set_xlabel('p_'+str(ax_ind)+' (GeV)')
                axes[ax_ind].set_yscale('log')
                axes[ax_ind].legend()
            ax[1][1].hist(list_pabs,bins=30,histtype='step',label=fl+' | |p|',density = True)
        ax[1][1].set_xlabel('|p| (GeV)')
        ax[1][1].set_yscale('log')
        ax[1][1].legend()
        plt.suptitle('E0 = '+str(self.Energy)+'GeV')
        if self.SAVE_MOM: fig2.savefig(FIG2+'_'+str(self.Energy)+'.png')

    def get_angles(self,pvec):
        px = pvec[0]
        py = pvec[1]
        pz = pvec[2]
        pabs = np.sqrt(px** 2 + py**2 + pz**2)
        #find sin/cos theta and phi
        sinTh = (np.sqrt(px**2 + py**2))/pabs
        cosTh = pz/pabs
        sinPh = py/(pabs*sinTh)
        cosPh = px/(pabs*sinTh)
        #find theta
        theta = np.arccos(cosTh)
        if sinTh < 0.: theta = -theta
        #theta += np.pi
        #find phi
        phi = np.arccos(cosPh)
        if sinPh < 0.: phi = -phi
        #phi += np.pi
        return theta, phi


    def get_sum_std_vecs_ev(self,ev):
        mom_list = self.get_momenta_flav_ev('any',ev)
        numparts = len(mom_list)
        avg_px = 0.
        avg_px2 = 0.
        avg_py = 0.
        avg_py2 = 0.
        avg_pz = 0.
        avg_pz2 = 0.
        for mom in mom_list:
            avg_px += mom[0]
            avg_px2 += mom[0]**2
            avg_py += mom[1]
            avg_py2 += mom[1]**2
            avg_pz += mom[2]
            avg_pz2 += mom[2]**2
        avg_px /= numparts
        avg_px2 /= numparts
        avg_py /= numparts
        avg_py2 /= numparts
        avg_pz /= numparts
        avg_pz2 /= numparts
        std_px = np.sqrt(avg_px2 - avg_px**2)
        std_py = np.sqrt(avg_py2 - avg_py**2)
        std_pz = np.sqrt(avg_pz2 - avg_pz**2)
        avg_vec = [avg_px,avg_py,avg_pz]
        std_vec = [std_px,std_py,std_pz]
        return  avg_vec, std_vec ### avgvec is 

    def get_cone_Rpsi_avg(self):
        #FIG4 = 'jet_cone'
        #fig4 = plt.figure(FIG4,figsize=(20, 10))
        #ax1 = fig4.add_subplot(221)
        #ax2 = fig4.add_subplot(222)
        numevs = len(self.map)
        avgR = 0.
        avgPsi = 0.
        R_vec = []
        Psi_vec = []
        for ev in range(1,numevs+1):
            avg_p, std_p = self.get_sum_std_vecs_ev(ev)
            rvec = []
            r_dot_p = 0.
            r2 = 0.
            p2 = 0.
            for comp in range(len(avg_p)):
                rvec.append(avg_p[comp] + std_p[comp])
                r_dot_p += avg_p[comp]*rvec[-1]
                r2 += rvec[-1]**2
                p2 += avg_p[comp]**2
            abs_r = np.sqrt(r2)
            abs_p = np.sqrt(p2)
            cosPs = r_dot_p/(abs_p*abs_r)
            sinPs = np.sqrt(1. - cosPs**2)
            R = abs_r*sinPs
            psi = np.arccos(cosPs)
            R_vec.append(R)
            avgR += R
            Psi_vec.append(psi)
            avgPsi += psi
        avgR /= numevs
        avgPsi /= numevs
        #ax3.set_xlim(0.,1.)
        #ax3.set_ylim(0.,1.)
        #ax1.hist(R_vec,bins=30,histtype='step',density = True)
        #ax1.set_xlabel('R')
        #ax2.hist(Psi_vec,bins=30,histtype='step',density = True)
        #ax2.set_xlabel('\Psi')
        #if self.SAVECONESTUFF: fig4.savefig(FIG4+'_'+str(self.Energy)+'.png')
        return avgR, avgPsi

    def do_angles_stuff(self):
        #FIG3 = 'angles'
        #fig3, ax = plt.subplots(1,2,figsize=(24, 12))
        #ax[0] = fig3.add_subplot(121)#, projection='polar')
        #ax[1] = fig3.add_subplot(122, projection='polar')
        for fl in quark_types:
            momenta = self.get_mom_dist_flav(fl)
            theta_list = []
            phi_list = []
            for mom in momenta:
                theta, phi = self.get_angles(mom)
                theta_list.append(theta)
                phi_list.append(phi)
            if fl == 'NaN': fl = 'g'
            #ax[0].hist(theta_list,bins=30,histtype='step',label=fl,density = True)
            #ax[1].hist(phi_list,bins=40,histtype='step',label=fl,density = True)
            #ax[0].set_xlabel('theta')
            #ax[1].set_xlabel('phi')
        #for i in range(0,len(ax)):
        #    ax[i].legend()
        #fig3.suptitle('E0 = '+str(self.Energy)+'GeV')
        #if self.SAVE_ANG: fig3.savefig(FIG3+'_'+str(self.Energy)+'_'+self.initial_flavour+'.png')
        return theta_list, phi_list