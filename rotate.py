import numpy as np
import matplotlib.pyplot as plt

def R(vec,axis,delta = 'nan',sin = 'nan',cos = 'nan'):
    ax_map = {'x': 0, 'y': 1, 'z': 2}
    ax_ind = ax_map[axis]
    if (sin == 'nan' and cos == 'nan') and delta != 'nan':
        cosine = np.cos(delta)
        sine = np.sin(delta)
    elif (sin != 'nan' and cos != 'nan') and delta == 'nan':
        cosine = cos
        sine = sin
    else:
        print('rotation went wrong!')
        return 'nan'
    sgn = 1.
    if ax_ind%2 != 0: sgn = -sgn
    rot_mat = [[cosine, -sgn*sine],
               [sgn*sine  , cosine]]
    subvec = []
    subvec_correspondance = {}
    for vec_ind in range(len(vec)):
        if vec_ind != ax_ind:
            subvec.append(vec[vec_ind])
            subvec_correspondance[vec_ind] =  len(subvec) - 1## gives the index in the subvector for a given index in the original one
    new_sub_vec = []
    for line in range(len(subvec)):
        new_entry = 0.
        for col in range(len(subvec)):
            new_entry += rot_mat[line][col]*subvec[col]
        new_sub_vec.append(new_entry)
    new_vec = np.zeros(3)
    for vec_ind in range(len(vec)):
        if vec_ind == ax_ind:
            new_vec[vec_ind] = vec[vec_ind]
        else:
            new_vec[vec_ind] = new_sub_vec[subvec_correspondance[vec_ind]]
    return list(new_vec)

#delta = np.pi/4.
#px = 0.
#py = 1.
#pz = 1.
#old = [px,py,pz]
#new = R(old,'y',cos = np.cos(delta),sin = np.sin(delta))#delta)
#print(old)
#print(new)
#print(R(new,'y',-delta))

