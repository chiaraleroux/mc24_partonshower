import numpy as np
import matplotlib.pyplot as plt
import rotate

np.random.seed(0)
#print(np.random.uniform(0.,1.))
#print(np.random.uniform(0.,1.))
#print(np.random.uniform(0.,1.))
Lqcd = 0.2 # 200MeV = 0.2 GeV
ALPHA0 = 12.*np.pi/(33-2*5)
ptCUTOFF = 1.
quark_types = ['u','d','s','c','b']
quark_masses = {'u': 2.3,'d': 4.8,'s': 95.,'c': 1275.,'b': 4180., 'NaN': 0.}
#quark_masses = {'u': 0.,'d': 4.8,'s': 95.,'c': 1275.,'b': 4180., 'NaN': 0.}

class particle:
    def __init__(self,mother,E,type,momentum,alive = True,flavour = 'NaN'):
        self.mother = mother
        self.alive = alive
        self.energy = E
        self.momentum = momentum
        self.type = type
        if self.type == 'q' and (flavour not in quark_types):
            print('flavour error! will change it to nan')
            self.flavour = 'NaN'
        else:
            self.flavour = flavour
        self.mass = quark_masses[self.flavour]
        if self.energy**2 < self.mass**2:
            print('INVALID ENERGY FOR THIS QUARK MASS')
    
    def get_momentum(self):
        return self.momentum

    def get_momentum_comp(self,comp):
        return self.momentum[comp]

    def get_abs_momentum(self):
        val = 0.
        for comp in self.momentum:
            val += comp**2
        return val
    
    def get_flavour(self):
        return self.flavour

    def get_mass(self):
        return self.mass
    
    def can_decay(self):
        return self.energy >= 2.*ptCUTOFF
    
    def get_energy(self):
        return self.energy
    
    def get_mother(self):
        return self.mother
    
    def get_alive(self):
        return self.alive
        
    def get_type(self):
        return self.type
    
    def kill(self):
        self.alive = False
    
    def __str__(self):
        prtstr = 'mother = '+str(self.mother)
        prtstr += ' | alive = '+str(self.alive)
        prtstr += ' | energy = '+str(self.energy)
        prtstr += ' | flavour = '+str(self.flavour)
        prtstr += ' | momentum = '+str(self.momentum)
        prtstr += ' | type = '+self.type+'\n'
        return prtstr

class EventRecord():
    def __init__(self):
        self.particleslist = []
    
    def get_particle_list(self):
        return self.particleslist
    
    def add(self,particle):
        self.particleslist.append(particle)
    
    def get_particle(self,index):
        return self.particleslist[index]

    def get_size(self):
        return len(self.particleslist)
    
    def update_part_at_index(self,index,newparticle):
        self.particleslist[index] = newparticle
    
    def __str__(self):
        outstr = ''
        for particle in self.particleslist:
            outstr += str(particle)
        return outstr

def heav(x):
    if x >= 0.:
        return 1.
    else:
        return 0.

def Pqqg(z):
    return (4./3.)*(1. + z**2)/(1. - z)

def PqqgHat(z):
    return 8./(3.*(1. - z))

def Pggg(z):
    return 3.*(1. - z*(1. - z))**2/(z*(1. - z))

def PgggHat(z):
    return 3./(z*(1.-z))

def Pgqq(z):
    return (5./2.)*(z**2 + (1. - z)**2)

def PgqqHat(z):
    return (5./2.)*(3. - 2.*z)

def PHat(z,type_split):
    if type_split == 'qqg':
        return PqqgHat(z)
    elif type_split == 'ggg':
        return PgggHat(z)
    elif type_split == 'gqq':
        return PgqqHat(z)

def PHat_Prim(z,type_split):
    if type_split == 'qqg':
        return (-8./3.)*np.log(1. - z)
    elif type_split == 'ggg':
        return np.log(z) - np.log(1-z)
    elif type_split == 'gqq':
        return (5./2.)*(3.*z - z**2)

def PHat_Prim_inv(y,type_split):
    if type_split == 'qqg':
        return 1. - np.exp(-3.*y/8.)
    elif type_split == 'ggg':
        return np.exp(y)/(1. + np.exp(y))
    elif type_split == 'gqq':
        return (1./2.)*(3. - np.sqrt(9. - 8.*y/5.))

def Ktil(type_split,Ea): #integral of Phat_type from zmin = ptcutoff/Ea to zmaxx = 1 - ptcutoff/Ea
    zmin = ptCUTOFF/Ea
    zmax = 1. - zmin
    return PHat_Prim(zmax,type_split) - PHat_Prim(zmin,type_split)

def g2(z,type_split,Ea):
    phat = PHat(z,type_split)
    pref = ALPHA0/(2*np.pi)
    return pref*phat*heav(z-ptCUTOFF/Ea)*heav(1 - ptCUTOFF/Ea - z)

def g1(pt2):
    return (1./pt2)*(1./np.log(pt2/(Lqcd**2)))

def f(pt2,z,type_split,EA):
    pt = np.sqrt(pt2)
    if type_split == 'qqg':
        Pabc = Pqqg(z)
    elif type_split == 'ggg':
        Pabc = Pggg(z)
    elif type_split == 'gqq':
        Pabc = Pgqq(z)
    pref = ALPHA0/(2.*np.pi)
    ptpart = (1./pt2)*(1./np.log(pt2/(Lqcd**2)))
    return pref*ptpart*Pabc*heav(z - pt/EA)*heav(1. - pt/EA - z)

def g(pt2,z,type_split,Ea):
    return g1(pt2)*g2(z,type_split,Ea)

def sample_z(type_split,Ea):
    R = np.random.uniform(0.,1.)
    zmin = ptCUTOFF/Ea
    Y = R * Ktil(type_split,Ea) + PHat_Prim(zmin,type_split)
    return PHat_Prim_inv(Y,type_split)

def sample_pt2(pt2max,type_split,Ea):
    ymax = np.log(np.log(pt2max/(Lqcd**2)))
    R = np.random.uniform(0.,1.)
    pref = ALPHA0/(2.*np.pi*Lqcd**2)
    K = Ktil(type_split,Ea) * pref
    y = (ymax + np.log(R)/K)
    #print(y,ymax,np.log(np.log(pt2max/(1**2))))
    pt2 = (Lqcd**2)*(np.exp(np.exp(y)))
    #print(pt2)
    return pt2 #pt2max*(np.exp(np.log(R)/K))

def rotateBack(pA,pDaughterPrime):
    pAx = pA[0]
    pAy = pA[1]
    pAz = pA[2]
    pAabs = np.sqrt(pAx**2 + pAy**2 + pAz**2)
    sinTh = (np.sqrt(pAx**2 + pAy**2))/pAabs
    cosTh = pAz/pAabs
    sinPh = pAy/(pAabs*sinTh)
    cosPh = pAx/(pAabs*sinTh)
    pDghtPrimePrime = rotate.R(pDaughterPrime,'y',sin=sinTh,cos=cosTh)
    pDght = rotate.R(pDghtPrimePrime,'z',sin=sinPh,cos=cosPh)
    return pDght

numberOfEvents = 1000
initialEs = [50.,100.,200.]#,10000]
#initialEs = [5000.,10000]
initial_flavour = 'NaN'
initial_type = 'g'
initial_pt2 = 0
all_picked_pt2s_zs = []
for Einit in initialEs:
    print('starting energy = ',Einit)
    recordslist = []
    avg_splts = 0.
    for ev in range(numberOfEvents):
        if ev % 1 == 100: print('starting event ',ev)
        record = EventRecord()
        E0 = Einit
        mAinit = quark_masses[initial_flavour]
        phi = np.random.uniform(0.,2*np.pi)
        px = np.sqrt(initial_pt2)*np.cos(phi)
        py = np.sqrt(initial_pt2)*np.sin(phi)
        pz2 = E0**2 - mAinit**2 - px**2 - py**2
        if pz2 >= 0.:
            pz = np.sqrt(pz2)
        else:
            pz = 0.
            print('particle probably too heavy or pt2 initial too high')
        p_init = [px,py,pz]
        particle0 = particle(-1,E0,initial_type,flavour=initial_flavour,momentum=p_init)
        record.add(particle0)
        record_it = 0
        numsplits = 0.
        while record_it < record.get_size():
            #print(record_it)
            ### take care of decay a - > b + c
            ### start by getting some information from a to know where to begin
            partA = record.get_particle(record_it)
            #print(partA)
            EA = partA.get_energy()
            Atype = partA.get_type()
            Aflav = partA.get_flavour()
            Amom = partA.get_momentum()
            pt2max = (EA/2.)**2
            pt2max_per_type = {}
            if Atype == 'q':
                split_types = {'qqg'}
            elif Atype == 'g':
                split_types = {'ggg', 'gqq'}
            for typ in split_types:
                pt2max_per_type[typ] = pt2max
            ### now let's define some things to save some values
            possible_decayS = []
            provisional_part_Cs = {} ## if some split type is accepted saves the daugher c here
            provisional_part_Bs = {} ## if some split type is accepted saves the daugher b here
            provisional_part_pt2 = {} ## if some split type is accepted saves the pt2
            provisional_part_z = {} ## if some split type is accepted saves the z
            ### got all I need, let's find a decay (if a can actually decay)
            if not partA.can_decay():
                record_it += 1
            else:
                #finding the decay(s)! first, we still haven't found it, bummers
                got_decay = False
                give_up = False
                attempts = 0
                MAXATTEMPTS = 1000000
                while (not got_decay) and not give_up:# and attempts < MAXATTEMPTS:
                    attempts += 1
                    #print(record_it,pt2max_per_type)
                    if attempts >= MAXATTEMPTS:
                        print('reaches')
                        record_it += 1
                        give_up = True
                        break
                    #print(ev,record_it,attempts)
                    ### let's find POSSIBLE decays, so looping trhough split types
                    for spl_tp in split_types:
                        current_pt2_max = pt2max_per_type[spl_tp]
                        if current_pt2_max > ptCUTOFF**2:
                            newPt2 = sample_pt2(current_pt2_max,spl_tp,EA)
                            pt2max_per_type[spl_tp] = newPt2 ## even if it gets rejected, update max to do veto
                            newZ = sample_z(spl_tp,EA)
                            ratio = f(newPt2,newZ,spl_tp,EA)/g(newPt2,newZ,spl_tp,EA)
                            Rand = np.random.uniform(0.,1.)
                            if Rand < ratio:
                                ### if it get's accepted we check the kinematics
                                ### start by picking flavors and type for daughters
                                if spl_tp == 'qqg':
                                    bType, cType = Atype, 'g'
                                    bflav, cflav = Aflav, 'NaN'
                                elif spl_tp == 'ggg':
                                    bType = cType = 'g'
                                    cflav = bflav = 'NaN'
                                elif spl_tp == 'gqq':
                                    bType, cType = 'q', 'q'
                                    flavour = list(quark_types)[np.random.randint(0,len(quark_types))]
                                    cflav = bflav = flavour
                                ### now can get the masses to see kinematics finally
                                bmass = quark_masses[bflav]
                                cmass = quark_masses[cflav]
                                Eb = EA*newZ
                                Ec = EA*(1. - newZ)
                                pzB2 = Eb**2 - bmass**2 - newPt2
                                pzC2 = Ec**2 - cmass**2 - newPt2
                                if pzB2 >= 0. and pzC2 >= 0.: ## at least that..! no conservation but oh well
                                    phi = np.random.uniform(0.,2.*np.pi)
                                    px = np.sqrt(newPt2)*np.cos(phi)
                                    py = np.sqrt(newPt2)*np.sin(phi)
                                    pzB = np.sqrt(pzB2)
                                    pzC = np.sqrt(pzC2)
                                    pBprime = [px,py,pzB]
                                    pB = pBprime#rotateBack(Amom,pBprime)
                                    pCprime = [-px,-py,pzC]
                                    pC = pCprime#rotateBack(Amom,pCprime)
                                    provisional_part_pt2[spl_tp] = newPt2
                                    provisional_part_z[spl_tp] = newZ
                                    possible_decayS.append(spl_tp)
                                    partB = particle(record_it,Eb,bType,momentum=pB,flavour=bflav)
                                    provisional_part_Bs[spl_tp] = partB
                                    partC = particle(record_it,Ec,cType,momentum=pC,flavour=cflav)
                                    provisional_part_Cs[spl_tp] = partC
                    ### ok, checked all split types and saved the possible decays
                    ### let's see who wins
                    num_poss_decays = len(possible_decayS)
                    if num_poss_decays == 0: ### no types of decay was accepted
                        give_up = True #maybe it's final state, lets check if we can continue evolving in pt2
                        for spl_tp in pt2max_per_type:
                            if pt2max_per_type[spl_tp] > ptCUTOFF**2:
                                give_up = False #there's still hope!
                    elif num_poss_decays == 1: ## well, if no one else wants then this one will take it!
                        got_decay = True
                        chosen_decay = possible_decayS[0]
                    else: ## uuh plenty of candidates... winner takes all!
                        largest_pt2 = 0.
                        earliest_split = ''
                        for spl_tp in pt2max_per_type:
                            if (spl_tp in possible_decayS) and (pt2max_per_type[spl_tp] > largest_pt2):
                                ### this spl_tp is possible
                                ### so if pt2 from this one is the largest, it wins
                                largest_pt2 = pt2max_per_type[spl_tp]
                                earliest_split = spl_tp
                        ### finished iterating the pt2 values so earliest_split is the winner:
                        got_decay = True
                        chosen_decay = earliest_split
                    if got_decay: ## cool, kill a, add daughters to record and move on!
                        numsplits+=1
                        partA.kill()
                        record.update_part_at_index(record_it,partA)
                        chosen_pt2 = provisional_part_pt2[chosen_decay]
                        chosen_z = provisional_part_z[chosen_decay]
                        chosen_partB = provisional_part_Bs[chosen_decay]
                        chosen_partC = provisional_part_Cs[chosen_decay]
                        record.add(chosen_partB)
                        record.add(chosen_partC)
                        record_it += 1
                        all_picked_pt2s_zs.append([EA,chosen_decay,chosen_pt2,chosen_z])
                    elif give_up:
                        record_it += 1
        avg_splts += numsplits
        recordslist.append(record)
    avg_splts /= numberOfEvents
    print('Einit = ',Einit,' => avgsplts = ',avg_splts)
    SAVE = True
    if SAVE:
        outrecord = open('./fix/records_'+str(initial_flavour)+'_'+str(Einit)+'.dat','w')
        for rec in recordslist:
            outrecord.write(str(rec))
        #outrecord.write('END') ### weeeeird! changes the result.. uses some random num??
        outrecord.write('END\n')
        outrecord.close()

if SAVE:
    sampligfile = open('./fix/sampling_results_'+str(initial_flavour)+'.dat','w')
    for entry in all_picked_pt2s_zs:
        string = str(entry[0]) + '\t'
        string += entry[1] + '\t'
        string += str(entry[2]) + '\t'
        string += str(entry[3]) + '\t'
        sampligfile.write(string+'\n')
    sampligfile.close()