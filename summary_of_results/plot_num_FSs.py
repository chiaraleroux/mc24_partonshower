import numpy as np
import matplotlib.pyplot as plt

file_splits = open('./num_FSs.dat','r')
part_types = ['u','d','s','c','b','g']
energies = [50,100,1000,2000,5000,10000]

full_map = {}
for line in file_splits:
    type0, E0, u, d, s, c, b, g = line.split()
    tot = float(u) + float(d) + float(s) + float(c) + float(b) + float(g)
    values_map = {'tot':tot,
                  'u':float(u),
                  'd':float(d),
                  's':float(s),
                  'c':float(c),
                  'b':float(b),
                  'g':float(g)}
    if type0 == 'NaN': type0 = 'g'
    full_map[(type0,float(E0))] = values_map


#for tup in full_map:
#    print('\n',tup,'\n',full_map[tup])
mode = 'frac' # 'frac' or 'abs'
for part in part_types:
    print('========== ',part,' ===========')
    x_axis = []
    for tup in full_map:
        if tup[0] == part:
            x_axis.append((tup[1]))
    x_axis.sort()
    x_labels = []
    for E in x_axis: x_labels.append(str(E))
    yaxis = {}
    for ptype in part_types:
        yaxis[ptype] = []
        bottom = []
        for E in x_axis:
            bottom.append(0.)
            if mode == 'frac':
                yaxis[ptype].append(full_map[(part,E)][ptype]/full_map[(part,E)]['tot'])
            elif mode == 'abs':
                yaxis[ptype].append(full_map[(part,E)][ptype])
    fig1 = plt.figure()#figsize=(8,8))
    ax1 = fig1.add_subplot(111)
    #print(x_axis)
    #print(yaxis)
    for ptype, values in yaxis.items():
        ax1.bar(x_labels,values,width=0.5,label=ptype,bottom=bottom)
        bottom = list(np.array(bottom) + np.array(values))
    ax1.legend()
    ax1.set_xlabel('Initial energy (GeV)')
    ax1.set_ylabel('number of final state particles')
    ax1.set_title('initial particle: '+part)
    plt.savefig('num_FS_hist_'+part+'_mode_'+str(mode)+'.png')
