import numpy as np
import matplotlib.pyplot as plt

file_splits = open('./num_splits.dat','r')
part_types = ['u','d','s','c','b','g']
energies = [50,100,1000,2000,5000,10000]

full_map = {}
for line in file_splits:
    type0, E0, tot, qqg, ggg, gqq = line.split()
    values_map = {'tot':float(tot),
                  'qqg':float(qqg),
                  'ggg':float(ggg),
                  'gqq':float(gqq)}
    if type0 == 'NaN': type0 = 'g'
    full_map[(type0,float(E0))] = values_map


#for tup in full_map:
#    print('\n',tup,'\n',full_map[tup])
mode = 'frac' #or 'abs'
for part in part_types:
    x_axis = []
    split_types = []
    for tup in full_map:
        if tup[0] == part:
            x_axis.append((tup[1]))
            for split in list(full_map[(tup)].keys()):
                if split not in split_types and split != 'tot': split_types.append(split)
    yaxis = {}
    x_axis_labels = []
    x_axis.sort()
    for E in x_axis: x_axis_labels.append(str(E))
    for split in split_types:
        yaxis[split] = []
        bottom = []
        for E in x_axis:
            bottom.append(0.)
            if mode == 'frac':
                yaxis[split].append(full_map[(part,E)][split]/full_map[(part,E)]['tot'])
            elif mode == 'abs':
                yaxis[split].append(full_map[(part,E)][split])
    fig1 = plt.figure(figsize=(8,8))
    ax1 = fig1.add_subplot(111)
    for split, values in yaxis.items():
        ax1.bar(x_axis_labels,values,width=0.5,label=split,bottom=bottom)
        bottom = list(np.array(bottom) + np.array(values))
    ax1.legend()
    ax1.set_xlabel('Initial energy (GeV)')
    ax1.set_ylabel('number of splittings')
    ax1.set_title('initial particle: '+part)
    plt.savefig('splittings_hist_'+part+'_mode_'+str(mode)+'.png')
