import numpy as np
import matplotlib.pyplot as plt
import os

part_types = ['u','d','s','c','b','NaN']
energies = [50,100,1000,2000,5000,10000]
folder = './E_scpectra/Edist_'
for init_part in part_types:
    fig = plt.figure(figsize=(30,10))
    ax_map = {}
    i=0
    for E0 in energies:
        spec_per_fs = {}
        for fs_part in part_types:
            filename = folder+str(E0)+'.0_'+init_part+'_'+fs_part+'.dat'
            if os.path.exists(filename):
                file = open(filename,'r')
                print(filename,' exists')
                evals = []
                for line in file:
                    evals.append(float(line))
                spec_per_fs[fs_part] = evals
            else:
                pass
                #print(filename,'is not here')
        if len(spec_per_fs) > 0:
            i+=1
            pos = int('13'+str(i))
            print(i,pos)
            ax_map[E0] = fig.add_subplot(pos)#'11'+str(i))
            for fs in spec_per_fs:
                print('hist ',init_part,E0,fs)
                if fs == 'NaN': lab = 'g'
                else: lab = fs
                ax_map[E0].hist(spec_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax_map[E0].legend()
            ax_map[E0].set_yscale('log')
            ax_map[E0].set_xlabel('Energy (GeV)')
            ax_map[E0].set_title('E0 = '+str(E0)+' (GeV)')
    fig.suptitle('initial particle: '+init_part)
    fig.savefig('e_spec_'+init_part+'.png')
