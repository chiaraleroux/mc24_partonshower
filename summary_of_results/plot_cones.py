import numpy as np
import matplotlib.pyplot as plt
import os

def get_angles(pvec):
    px = pvec[0]
    py = pvec[1]
    pz = pvec[2]
    pabs = np.sqrt(px** 2 + py**2 + pz**2)
    #find sin/cos theta and phi
    sinTh = (np.sqrt(px**2 + py**2))/pabs
    cosTh = pz/pabs
    sinPh = py/(pabs*sinTh)
    cosPh = px/(pabs*sinTh)
    #find theta
    theta = np.arccos(cosTh)
    if sinTh < 0.: theta = -theta
    #theta += np.pi
    #find phi
    phi = np.arccos(cosPh)
    if sinPh < 0.: phi = -phi
    #phi += np.pi
    return pabs, theta, phi

init_part_types = ['u','d','c','NaN']
energies = [50,100,1000,2000,5000,10000]
folder = './cones/cones_'
for init_part in init_part_types:
    fig = plt.figure(figsize=(30,10))
    ax_map = {}
    i=0
    avgps_per_E0 = {}
    stdps_per_E0 = {}
    for E0 in energies:
        filename = folder+str(E0)+'.0_'+init_part+'.dat'
        if os.path.exists(filename):
            file = open(filename,'r')
            print(filename,' exists')
            avg_ps = []
            std_ps = []
            for line in file:
                entries = line.split()
                avg_ps.append([float(entries[0]),float(entries[1]),float(entries[2])])
                std_ps.append([float(entries[3]),float(entries[4]),float(entries[5])])
            avgps_per_E0[E0] = avg_ps
            stdps_per_E0[E0] = std_ps
        else:
            pass
            #print(filename,'is not here')
    if len(avgps_per_E0) > 0:
        x0 = y0 = z0 = 0.
        ax = fig.add_subplot(111)
        for E0 in avgps_per_E0:
            i+=1
            pos = int('13'+str(i))
            #print(i,pos)
            #ax_map[E0] = fig.add_subplot(pos)#'11'+str(i))
            print('hist ',init_part,E0)
            if init_part == 'NaN': lab = 'g'
            else: lab = init_part
            avg_p = [0.,0.,0.]
            avg_p2 = [0.,0.,0.]
            for p in avgps_per_E0[E0]:
                avg_p = list(np.array(avg_p)+np.array(p))
                avg_p2 = list(np.array(avg_p2)+np.array(p)**2)
            avg_p = list(np.array(avg_p)/len(avgps_per_E0[E0]))
            avg_p2 = list(np.array(avg_p2)/len(avgps_per_E0[E0]))
            std_avg_p = list(np.array(avg_p2) - np.array(avg_p)**2)
            avg_std_p = [0.,0.,0.]
            for p in stdps_per_E0[E0]:
                avg_std_p = list(np.array(avg_std_p)+np.array(p))
            avg_std_p = list(np.array(avg_std_p)/len(stdps_per_E0[E0]))
            print('\t<p>=',avg_p)
            print('\tstd_<p>=',std_avg_p)
            print('\t<std_p>=',avg_std_p)
            cone_vec = list(np.array(avg_p)+np.array(avg_std_p))
            print('\tcone=',cone_vec)
            if (i==1):
                labcone = '<p> + <stddev(p)>'
                labp = '<p>'
            else:
                labcone = ''
                labp = ''
            coneD, coneTh, conePh = get_angles(cone_vec)
            xvec_cone = [x0,x0 + coneD*np.cos(coneTh),x0 + coneD*np.cos(coneTh)]
            yvec_cone = [0.,coneD*np.sin(coneTh),0.]
            ax.fill_between(xvec_cone,yvec_cone,list(-np.array(yvec_cone)),color='purple',alpha=0.2)
            #ax_map[E0].plot(xvec_cone,yvec_cone,color='b')#spec_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax.plot(xvec_cone,yvec_cone,'--',color='purple',label=labcone)#spec_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax.plot(xvec_cone,list(-np.array(yvec_cone)),'--',color='purple')#spec_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            avgpD, avgpTh, avgpPh = get_angles(avg_p)
            ax.text(x0 + coneD*np.cos(coneTh)/2.,-0.,'E0 = '+str(E0)+' GeV\ntheta='+'%.2f'%coneTh,fontsize='x-large',bbox=dict(boxstyle='round', facecolor='blue', alpha=0.2))
            xvec_avg_p = [x0,x0 + avgpD*np.cos(avgpTh)]
            yvec_avg_p = [0.,avgpD*np.sin(avgpTh)]
            x0 += coneD*np.cos(coneTh) + 1.
            ax.plot(xvec_avg_p,yvec_avg_p,color='b',label=labp)#spec_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax.legend()
            ax.set_xlabel('<px> + px0 (GeV)')
            ax.set_ylabel('<py> + py0 (GeV)')
            #ax.set_ylim(-75.,75.)
            #ax.set_xlim(0.,290.)
            #ax_map[E0].set_title('E0 = '+str(E0)+' (GeV)')
    fig.suptitle('initial particle: '+lab)
    fig.savefig('test_cone_'+init_part+'.png')
