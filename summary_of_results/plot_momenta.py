import numpy as np
import matplotlib.pyplot as plt
import os

part_types = ['u','d','s','c','b','NaN']
energies = [50,100,1000,2000,5000,10000]
folder = './p_spectra/pdist_'
for init_part in part_types:
    figpabs = plt.figure(figsize=(30,10))
    figth = plt.figure(figsize=(30,10))
    figph = plt.figure(figsize=(30,10))
    ax_map_pabs = {}
    ax_map_th = {}
    ax_map_ph = {}
    i=0
    for E0 in energies:
        px_per_fs = {}
        py_per_fs = {}
        pz_per_fs = {}
        th_per_fs = {}
        ph_per_fs = {}
        pabs_per_fs = {}
        for fs_part in part_types:
            filename = folder+str(E0)+'.0_'+init_part+'_'+fs_part+'.dat'
            #print(filename,os.path.exists(filename))
            if os.path.exists(filename):
                file = open(filename,'r')
                #print(filename,' exists')
                pxvals = []
                pyvals = []
                pzvals = []
                thvals = []
                phvals = []
                pabsvals = []
                for line in file:
                    px, py, pz, th, ph = line.split()
                    pxvals.append(float(px))
                    pyvals.append(float(py))
                    pzvals.append(float(pz))
                    thvals.append(float(th))
                    phvals.append(float(ph))
                    pabsvals.append(np.sqrt(float(px)**2 + float(py)**2 + float(pz)**2))
                px_per_fs[fs_part] = pxvals
                py_per_fs[fs_part] = pyvals
                pz_per_fs[fs_part] = pzvals
                th_per_fs[fs_part] = thvals
                ph_per_fs[fs_part] = phvals
                pabs_per_fs[fs_part] = pabsvals
            else:
                pass
                #print(filename,'is not here')
        if len(px_per_fs) > 0:
            i+=1
            pos = int('13'+str(i))
            print(i,pos)
            ####pabs
            ax_map_pabs[E0] = figpabs.add_subplot(pos)#'11'+str(i))
            for fs in pabs_per_fs:
                print('hist ',init_part,E0,fs)
                if fs == 'NaN': lab = 'g'
                else: lab = fs
                ax_map_pabs[E0].hist(pabs_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax_map_pabs[E0].legend()
            ax_map_pabs[E0].set_yscale('log')
            ax_map_pabs[E0].set_xlabel('|p| (GeV)')
            ax_map_pabs[E0].set_title('E0 = '+str(E0)+' (GeV)')
            ####th
            ax_map_th[E0] = figth.add_subplot(pos)#'11'+str(i))
            for fs in th_per_fs:
                if fs == 'NaN': lab = 'g'
                else: lab = fs
                ax_map_th[E0].hist(th_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax_map_th[E0].legend()
            ax_map_th[E0].set_yscale('log')
            ax_map_th[E0].set_xlabel('theta')
            ax_map_th[E0].set_title('E0 = '+str(E0)+' (GeV)')
            ####ph
            ax_map_ph[E0] = figph.add_subplot(pos)#'11'+str(i))
            for fs in ph_per_fs:
                if fs == 'NaN': lab = 'g'
                else: lab = fs
                ax_map_ph[E0].hist(ph_per_fs[fs],bins = 20,label=lab,histtype='step',density=True)
            ax_map_ph[E0].legend()
            ax_map_ph[E0].set_yscale('log')
            ax_map_ph[E0].set_xlabel('phi')
            ax_map_ph[E0].set_title('E0 = '+str(E0)+' (GeV)')
    figpabs.suptitle('initial particle: '+init_part)
    figpabs.savefig('p_spec_'+init_part+'.png')
    figth.suptitle('initial particle: '+init_part)
    figth.savefig('th_spec_'+init_part+'.png')
    figph.suptitle('initial particle: '+init_part)
    figph.savefig('ph_spec_'+init_part+'.png')
