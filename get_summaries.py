import numpy as np
import matplotlib.pyplot as plt
import analyse
import os

quark_types = ['u','d','s','c','b','NaN']

'''
## get splits file(s)
num_splits_file = open('num_splits.dat','w')
num_fs_file = open('num_FSs.dat','w')
for f in os.listdir('./large_sts/'):
    if f.find('.dat') != -1:
        name = f.replace('.dat','')
        name = name.replace('records_','')
        qtype = name.split('_')[0]
        E0 = name.split('_')[1]
        #print(qtype,E0)
        if True:#qtype == 'u':
            anal = analyse.analysis(int(float(E0)),folder='./large_sts/',flavour=qtype,SAVE_ANG=False,SAVECONESTUFF = False)
            tot = anal.get_num_avg_splits_of_type('tot')
            qqg = anal.get_num_avg_splits_of_type('qqg')
            ggg = anal.get_num_avg_splits_of_type('ggg')
            gqq = anal.get_num_avg_splits_of_type('gqq')
            num_splits_file.write(qtype+'\t'+E0+'\t'+str(tot)+
                                    '\t'+str(qqg)+'\t'+str(ggg)+
                                    '\t'+str(gqq)+'\n')
            numFss = ''
            for typ in quark_types:
                avg_num = anal.get_avg_num_flavour(typ)
                numFss += str(avg_num)+'\t'
            numFss+='\n'
            num_fs_file.write(numFss)
num_splits_file.close()
num_fs_file.close()

## get p and E dist file(s)
for f in os.listdir('./large_sts/'):
    if f.find('.dat') != -1:
        name = f.replace('.dat','')
        name = name.replace('records_','')
        qtype = name.split('_')[0]
        E0 = name.split('_')[1]
        if True:#qtype == 'u':
            anal = analyse.analysis(int(float(E0)),folder='./large_sts/',flavour=qtype,SAVE_ANG=False,SAVECONESTUFF = False)
            for fla in quark_types:
                print('spectra for type ',fla)
                pdist_file = open('pdist_'+E0+'_'+qtype+'_'+fla+'.dat','w')
                fla_ps = anal.get_mom_dist_flav(fla)
                for pvec in fla_ps:
                    theta, phi = anal.get_angles(pvec)
                    line = str(pvec[0])+'\t'+str(pvec[1])+'\t'+str(pvec[2])+'\t'+str(theta)+'\t'+str(phi)+'\n'
                    pdist_file.write(line)
                pdist_file.close()
                Edist_file = open('Edist_'+E0+'_'+qtype+'_'+fla+'.dat','w')
                fla_Es = anal.get_energies_dist_flav(fla)
                for energy in fla_Es:
                    line = str(energy)+'\n'
                    Edist_file.write(line)
                Edist_file.close()

## get R psi
for f in os.listdir('./large_sts/'):
    if f.find('.dat') != -1:
        name = f.replace('.dat','')
        name = name.replace('records_','')
        qtype = name.split('_')[0]
        E0 = name.split('_')[1]
        if True:#qtype == 'u':
            anal = analyse.analysis(int(float(E0)),folder='./large_sts/',flavour=qtype,SAVE_ANG=False,SAVECONESTUFF = False)
            cones_file = open('cones_'+E0+'_'+qtype+'.dat','w')
            for ev in range(1,anal.num_evs + 1):
                if ev%500 == 0: print('doing event ',ev)
                avg_p, std_p = anal.get_sum_std_vecs_ev(ev)
                line = str(avg_p[0])+'\t'+str(avg_p[1])+'\t'+str(avg_p[2])+'\t'
                line += str(std_p[0])+'\t'+str(std_p[1])+'\t'+str(std_p[2])+'\n'
                cones_file.write(line)
            cones_file.close()
     '''
       